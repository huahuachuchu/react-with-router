import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router, Link} from 'react-router-dom';
import {Route} from "react-router";

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <div>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/about">My Profile</Link>
              </li>
              <li>
                <Link to="/topics">About Us</Link>
              </li>
            </ul>

            <Route exact path="/" component={Home} />
            <Route path="/about" component={MyProfile} />
            <Route path="/topics" component={AboutUs} />
          </div>
        </Router>
      </div>
    );
    function Home() {
      return (
        <div>
          <h2>This is a beautiful home page.</h2>
        </div>
      );
    }
    function MyProfile() {
      return (
        <div>
          <h2>Username:XXX</h2>
          <h2>Gender:Female</h2>
          <h2>Work:IT industry</h2>
          <h2>Website:'/my-profile'</h2>
        </div>
      );
    }

    function AboutUs() {
      return (
        <div>
          <h2>Company:Toughtworks Local</h2>
          <h2>Location:Xi'an</h2>
          <h2>For more information,please view our
            <p>
              <Link to="/">website</Link>
              <Route exact path="/" component={Home} />
              website
            </p>
          </h2>
        </div>
      );
    }

  }

}
export default App;
